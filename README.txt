
README for content_moderator.module:

Compatible with Drupal 4.7.  

WILL NOT BE UPDATED for 5.x in favor of the Modr8 module. 

A simple module with code adapted from the Drupal 4.7 node.module, and which 
reuses functionality from node.module where possible.  Allows you to give a 
"moderate nodes" permission to a user role.  Users with this permission may see 
a list of content (restricted using the sql_rewite so they cannot see content 
they are normally prohibited) at /admin/content_moderator.  Within this list 
they can either approve posts to move them out of moderation, or put them into 
moderation to remove them from public view.  Also, uses hook_nodeapi to prevent 
nodes from going back into moderation (if that's their default setting) 
when edited by a user with this permission.

A block is also made available to show the most recent ten posts in the 
moderation queue.

This module is intended to be useful for a site like Drupal.org which has a 
handbook where all users can add book pages, but those pages go into the 
moderation queue.  This modules allows the work of reviewing and approving those
posts to be delegated to additional users without giving out the all-powerful 
"administer nodes" permission.

INSTALLATION:

As usual, upload to the modules directly.  Log in and go to /admin/modules and 
enable the module.

Go to /admin/access to assign the new permission to one or more user roles.

The new menu item with show up in the navigation menu.

Go to /admin/blocks if you wish to enable the block.  The block will only be 
visible to users with this permission.

----

Written by Peter (pwolanin@drupal)

